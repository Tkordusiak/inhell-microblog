package com.tk.inHell.model;

import lombok.Getter;

public enum Status {
    ACTIVE("aktywny"),
    INACTIVE("nieaktywny"),
    BLOCKED("zablokowany"),
    ORIGINAL("orginalny"),
    EDITED("edytowany");

    @Getter
    private String description;

    Status( String description){
        this.description = description;
    }

}
