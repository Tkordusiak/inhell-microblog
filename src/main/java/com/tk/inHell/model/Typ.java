package com.tk.inHell.model;

import lombok.Getter;

public enum Typ {
    PUBLIC("publiczny"),
    PRIVATE("prywatny"),
    COMMENT("komentarz"),
    ENTRY("wpis");


    @Getter
    private String description;

   Typ(String description){
       this.description = description;
   }

}
