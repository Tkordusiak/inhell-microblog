package com.tk.inHell.model;

import javax.persistence.Table;
import java.sql.Date;

@Table(name = "messages")
public class Message {

    private String description;
    private Date dateOfCreate;
    private Status status;
    private Typ typ;
}
